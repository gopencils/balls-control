﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FlurrySDK;

public class FlurryAnalytics : MonoBehaviour
{
    public static FlurryAnalytics Instance;

#if UNITY_ANDROID
    [SerializeField] private string FLURRY_API_KEY;
#elif UNITY_IPHONE
    [SerializeField] private string FLURRY_API_KEY;
#else
    [SerializeField] private string FLURRY_API_KEY;
#endif

    private void Awake()
    {
        Instance = (Instance == null) ? this : Instance;
    }

    private void Start()
    {
        new Flurry.Builder()
                .WithCrashReporting(true)
                .WithLogEnabled(true)
                .WithLogLevel(Flurry.LogLevel.LogVERBOSE)
                .Build(FLURRY_API_KEY);
    }

    public void StartEvent()
    {
        Flurry.LogEvent("Event_Start");
    }

    public void EndEvent()
    {
        Flurry.LogEvent("Event_End");
    }
}
